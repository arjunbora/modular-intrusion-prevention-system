import MySQLdb
import time
import os
import thread
import datetime #ADDED

def get_sec(s):
    l = s.split(':')
    return int(l[0]) * 3600 + int(l[1]) * 60 + int(l[2])

def init():
    while(1):
        time.sleep(1)
        db = MySQLdb.connect(host="localhost", user="root", passwd="root", db="test1")
        cur = db.cursor()
        cur_time= datetime.datetime.now()
        cur_iso_time=cur_time.isoformat()
        select_to_unban_query = "select ip_address, application_name from banned_ips where isban=1 AND unban_time < '"+ cur_iso_time +"'"
        ip = cur.execute(select_to_unban_query)
        results = cur.fetchall()
        for row in results:
            delete_query = "delete from banned_ips where ip_address='"+row[0]+"'"
            delete_query2 ="delete from failed_logins where ip_address='"+row[0]+"' and application_name='"+row[1]+"' "
            cur.execute(delete_query)
            cur.execute(delete_query2)
            cur.execute("commit")
            unban_query = "iptables -D INPUT -s "+row[0]+" -j DROP"
            os.system(unban_query)
        select_query = "select ip_address, application_name, block_time from banned_ips where isban=0"
        ip = cur.execute(select_query)
        results = cur.fetchall()

        for row in results:
            ip = row[0]
            application = row[1]
            bantime = row[2]
            query = "iptables -A INPUT -s "+str(ip)+" -j DROP"
            delete_query2 ="delete from failed_logins where ip_address='"+row[0]+"' and application_name='"+str(application)+"' "
            cur.execute(delete_query2)
            cur.execute("commit")
            os.system(query)

            #print str(ip)
            #print str(application)
            #print str(bantime)
            update_query = "UPDATE banned_ips SET isban=1 WHERE ip_address='"+str(ip)+"' and application_name='"+str(application)+"' and block_time='"+str(bantime)+"'"     #DELETE WE CAN REMOVE ISBAN FROM DB TABLE
            cur.execute(update_query)
            cur.execute("commit")
        cur.close()
        del cur
        db.close()

init()
