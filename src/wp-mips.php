<?php
const SYSLOG_FACILITY = LOG_LOCAL1;
 
add_action('wp_login_failed', 'log_failed_attempt'); 
 
function log_failed_attempt( $username ) {
	openlog( 'wordpress('.$_SERVER['HTTP_HOST'].')', LOG_NDELAY|LOG_PID, SYSLOG_FACILITY);
	syslog( LOG_NOTICE, "Wordpress authentication failure for $username from {$_SERVER['REMOTE_ADDR']}" );
}
?>
