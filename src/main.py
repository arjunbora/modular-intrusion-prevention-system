import sys
import pyinotify
import MySQLdb
from threading import Thread, Condition
import patterns
import datetime

queue = []
condition = Condition()
files = {}
file_dict = {
    'ssh': '/var/log/auth.log',
    'wordpress': '/var/log/syslog',
	'joomla' : '/var/www/html/joomla/logs/error.php',
	'php' : '/var/log/apache2/error.log',
}
# This is for easy reverse lookup
reverse_file_dict = {}
fail_patterns = {
    'ssh': patterns.ssh_pattern,
    'wordpress': patterns.wordpress_pattern,
	'joomla' : patterns.joomla_pattern,
	'php': patterns.php_pattern,
}


def init():
    global file_dict
    for f_type, location in file_dict.iteritems():
        file = open(location, 'r')
        files[f_type] = file
        # seek to end of file
        for line in file:
            pass
        reverse_file_dict[location] = f_type


class WorkItem(object):
    def __init__(self, loginType, ip, username, timestamp, attempts, platform):
        # 0 for successful login, 1 for failed attempt
        self.loginType = loginType
        self.ip = ip
        self.username = username
        self.timestamp = timestamp
        self.attempts = attempts
        self.platform = platform


class MyEventHandler(pyinotify.ProcessEvent):
    def process_IN_MODIFY(self, event):
        global files
        global queue, condition

        #print "Modifying ", event.pathname
        if event.pathname not in reverse_file_dict:
            print "Untracked file ", event.pathname
            return
        f_type = reverse_file_dict[event.pathname]
        file = files[f_type]
        fp = fail_patterns[f_type]
        while True:
            line = file.readline()
            if not line:
                break
            match = fp.match(line)
            if not match:
                continue
            match_dict = match.groupdict()
            timestamp = match_dict['timestamp']
            ip = match_dict['ip']
            user = match_dict['user']
            attempts = 1
            if 'attempts' in match_dict:
                attempts = int(match_dict['attempts']) if match_dict['attempts']\
                    != None else 1
            workItemObject = WorkItem(1, ip, user, timestamp, attempts, f_type)
            condition.acquire()
            queue.append(workItemObject)
            if len(queue) == 1:
                condition.notify()
            condition.release()


class ConsumerThread(Thread):
    def run(self):
        global queue, condition
        while True:
            condition.acquire()
            db = MySQLdb.connect(host="localhost", user="root", passwd="root", db="test1")
            cur = db.cursor()
            if not queue:
                # put this thread to sleep
                condition.wait()
            workItemObject = queue.pop(0)
            #print "From consumer thread"
            #print workItemObject.username
            #print workItemObject.ip
            #print workItemObject.timestamp
            #print workItemObject.attempts
            #print workItemObject.platform
            # do something with this workItemObject
            cur_time= datetime.datetime.now()
            cur_iso_time=cur_time.isoformat()
            insert_query = "INSERT INTO failed_logins(ip_address, application_name, fail_time) " \
                           "VALUES('"+workItemObject.ip+"','"+workItemObject.platform+"','"+cur_iso_time+"')"
            for num in xrange(workItemObject.attempts):
                cur.execute(insert_query)
                cur.execute("commit")
            cur.close()
            del cur
            db.close()
            condition.release()

init()
wm = pyinotify.WatchManager()
notifier = pyinotify.Notifier(wm, MyEventHandler())
wm.add_watch(file_dict.values(), pyinotify.IN_MODIFY)
consumerThread = ConsumerThread()
consumerThread.start()
try:
    notifier.loop(daemonize=False,
                  pid_file='/tmp/pyinotify.pid', stdout='/tmp/pyinotify.log')
except pyinotify.NotifierError, err:
    print >> sys.stderr, err
