import re

ssh_pattern = re.compile(
    r'(?P<timestamp>\w{3,3}\s\d{2,2}\s\d\d:\d\d:\d\d).*:\s+(message repeated '\
    '(?P<attempts>\d+) times:\s+\[\s+)?Failed password for '\
    '(invalid user )?(?P<user>\w+) from (?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.'\
    '\d{1,3})\s+port\s+(?P<port>\d+)\s+(?P<protocol>\w+)')

joomla_pattern = re.compile(
    r'(?P<timestamp>\d{4,4}-\d\d-\d\dT\d\d:\d\d:\d\d\+\d\d:\d\d)\s+'\
    '(?P<priority>\w+)\s+(?P<ip>\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\s+joomlafailure'\
    '\s+Username\s+and\s+password\s+do\s+not\s+match(?P<user>)')

wordpress_pattern = re.compile(
    r'(?P<timestamp>\w{3,3}\s+\d{1,2}\s+\d\d:\d\d:\d\d)\s+(?P<machine>\S+)\s+'\
    '\S+\s+Wordpress\s+authentication\s+failure\s+for\s+(?P<user>\w+)\s+from\s+'\
    '(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')

# PHP pattern should go here!
php_pattern = re.compile(
    r'\[(?P<timestamp>\w{3,3}\s+\w{3,3}\s+\d{1,2}\s+\d\d:\d\d:\d\d\.\d{1,6}\s+\d{4,4})\]\s+'\
    '\[:error\]\s+\[pid\s+\d+\]\s+\[client\s+(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):\d+\]\s+'\
    'phpmyadmin:LoginFailed(?P<user>)')


'''
Date time formats:
    ssh: Nov 12 17:26:07
    wordpress: Nov 14 13:42:59
    joomla : 2015-11-13T02:50:50+00:00
    php: Fri Nov 20 01:35:17.454348 2015
'''
